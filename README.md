# Information about Resources

## straight forward

- skills: provides a list of skills [“python”, “ec2”, “java”]
- tags: returns tags [“easy”, “decision”, “meeting”]
- priorities: return a few priorities [“urgent”, “not urgent”]
- projects: [“project a”, “project b”]
 
## some logic
- date: provides next 10 week days
 
## team collaboration / possible dependency

- people: provides names and skills for these people [{jack”:[“python”, “ec2”]}, {“mary”: [“java”, “ec2”]}]
  - team that is going to develop this service will have to know which skills are available.

- availability: returns available people [“jack”, "mary"]
  - team that is going to implement this service will have to know which names are available
 
## depends on another service

- resources: returns available people with the required skill (will call “people”, and “availability”)
  - this service has an input (skill) which is different than others
  - “give me available people that has XYZ skill”
