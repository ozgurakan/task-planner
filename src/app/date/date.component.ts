import { Component, OnInit } from '@angular/core';
import { DateService } from './date.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {
  items: Array<string>;

  constructor(private dateService: DateService) { }

  ngOnInit() {
    this.dateService.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
