import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const serviceUrl = environment.urls.date;

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor(private http: HttpClient) { }

  get(): Observable<string[]> {
    if (serviceUrl.length === 0) {
      return of(['date mock 1', 'date mock 2']);
    }

    return this.http.get<string[]>(serviceUrl);
  }
}
