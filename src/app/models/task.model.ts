export class Task {
  constructor(
    public tag: string,
    public skill: string,
    public project: string,
    public priority: string,
    public date: string,
    public resource: string
  ) { }
}
