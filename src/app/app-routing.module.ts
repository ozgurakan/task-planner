import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { AppComponent } from './app.component';
import { TagsComponent } from './tags/tags.component';
import { SkillsComponent } from './skills/skills.component';
import { ProjectsComponent } from './projects/projects.component';
import { PrioritiesComponent } from './priorities/priorities.component';
import { DateComponent } from './date/date.component';
import { AvailabilityComponent } from './availability/availability.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { PeopleComponent } from './people/people.component';
import { ResourcesComponent } from './resources/resources.component';

const routes: Routes = [
  { path: 'task', component: TasksComponent },
  { path: 'details', component: TaskDetailsComponent },
  { path: 'tags', component: TagsComponent },
  { path: 'skills', component: SkillsComponent},
  { path: 'projects', component: ProjectsComponent },
  { path: 'priorities', component: PrioritiesComponent },
  { path: 'date', component: DateComponent },
  { path: 'availability', component: AvailabilityComponent },
  { path: 'people', component: PeopleComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: '', component: TasksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
