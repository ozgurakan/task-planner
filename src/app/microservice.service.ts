import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

const serviceUrl = '';

@Injectable({
  providedIn: 'root'
})
export class MicroserviceService {

  constructor(private http: HttpClient) { }

  get(): Observable<string[]> {
    if (serviceUrl.length === 0) {
      return of(['mock 1', 'mock 22']);
    }

    return this.http.get<string[]>(serviceUrl);
  }
}
