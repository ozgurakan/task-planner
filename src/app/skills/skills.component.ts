import { Component, OnInit } from '@angular/core';
import { SkillsService } from './skills.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  items: Array<string>;

  constructor(private service: SkillsService) { }

  ngOnInit() {
    this.service.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
