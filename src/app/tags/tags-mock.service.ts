import { Injectable } from '@angular/core';
import { TagsService } from './tags.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagsMockService extends TagsService {

  get(): Observable<string[]> {
    const tags = [
      'easy',
      'call',
      'meeting',
      'chore',
      'idea'
    ];

    return of(tags);
  }
}
