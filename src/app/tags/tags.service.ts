import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const serviceUrl = environment.urls.tags;

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  constructor(private http: HttpClient) { }

  get(): Observable<string[]> {
    if (serviceUrl.length === 0) {
      return of(['mock 1', 'mock 2']);
    }

    return this.http.get<string[]>(serviceUrl);
  }
}
