import { Component, OnInit } from '@angular/core';
import { TagsService } from './tags.service';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
  items: Array<string>;

  constructor(private service: TagsService) { }

  ngOnInit() {
    this.service.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
