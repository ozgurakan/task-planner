import { Component, OnInit } from '@angular/core';
import { PrioritiesService } from './priorities.service';

@Component({
  selector: 'app-priorities',
  templateUrl: './priorities.component.html',
  styleUrls: ['./priorities.component.scss']
})
export class PrioritiesComponent implements OnInit {
  items: Array<string>;

  constructor(private service: PrioritiesService) { }

  ngOnInit() {
    this.service.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
