import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const serviceUrl = environment.urls.priorities;

@Injectable({
  providedIn: 'root'
})
export class PrioritiesService {
  constructor(private http: HttpClient) { }

  get(): Observable<string[]> {
    if (serviceUrl.length === 0) {
      return of(['urgent mock', 'not urgent mock']);
    }

    return this.http.get<string[]>(serviceUrl);
  }
}
