import { Component, OnInit } from '@angular/core';
import { AvailabilityService } from './availability.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.scss']
})
export class AvailabilityComponent implements OnInit {
  items: Array<string>;

  constructor(private avService: AvailabilityService) { }

  ngOnInit() {
    this.avService.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
