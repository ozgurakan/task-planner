import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TasksComponent } from './tasks/tasks.component';
import { TagsComponent } from './tags/tags.component';
import { AvailabilityComponent } from './availability/availability.component';
import { SkillsComponent } from './skills/skills.component';
import { PrioritiesComponent } from './priorities/priorities.component';
import { DateComponent } from './date/date.component';
import { ProjectsComponent } from './projects/projects.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TagsService } from './tags/tags.service';
import { TagsMockService } from './tags/tags-mock.service';
import { PeopleComponent } from './people/people.component';
import { ResourcesComponent } from './resources/resources.component';
import { AvailabilityService } from './availability/availability.service';
import { DateService } from './date/date.service';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    TagsComponent,
    AvailabilityComponent,
    SkillsComponent,
    PrioritiesComponent,
    DateComponent,
    ProjectsComponent,
    TaskDetailsComponent,
    PeopleComponent,
    ResourcesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    { provide: TagsService, useClass: TagsService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
