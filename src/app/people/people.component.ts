import { Component, OnInit } from '@angular/core';
import { PeopleService } from './people.service';
import { Person } from './people.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  items: Array<Person>;

  constructor(private service: PeopleService) { }

  ngOnInit() {
    this.service.get().subscribe((data: Person[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
