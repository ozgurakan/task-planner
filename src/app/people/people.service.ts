import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const serviceUrl = environment.urls.people;


export interface Person {
  name: string;
  skills: string[];
}

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  constructor(private http: HttpClient) { }

  get(): Observable<Person[]> {
    if (serviceUrl.length === 0) {
      return of([
        {name: 'name1', skills: ['skill1', 'skill2']},
        {name: 'name2', skills: ['skill1', 'skill2']}
      ]);
    }

    return this.http.get<Person[]>(serviceUrl);
  }

}
