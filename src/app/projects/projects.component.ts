import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  items: Array<string>;

  constructor(private service: ProjectsService) { }

  ngOnInit() {
    this.service.get().subscribe((data: string[]) => {
      console.log(data);
      this.items = data;
    });
  }

}
