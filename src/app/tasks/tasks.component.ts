import { Component, OnInit } from '@angular/core';
import { TagsService } from '../tags/tags.service';
import { SkillsService } from '../skills/skills.service';
import { ProjectsService } from '../projects/projects.service';
import { PrioritiesService } from '../priorities/priorities.service';
import { DateService } from '../date/date.service';
import { ResourcesService } from '../resources/resources.service';
import { Task } from '../models/task.model';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  tags: string[];
  skills: string[];
  projects: string[];
  priorities: string[];
  dates: string[];
  resources: Array<string>;
  public selectedSkill: string;
  public tasks: Array<Task>;

  constructor(
    private tagsService: TagsService,
    private skillsService: SkillsService,
    private projectsService: ProjectsService,
    private prioritiesService: PrioritiesService,
    private datesService: DateService,
    private resourcesService: ResourcesService
  ) {
    this.selectedSkill = '';
    this.tasks = [];
  }

  model = new Task('', '', '', '', '', '');

  ngOnInit() {
    this.tagsService.get().subscribe((data: string[]) => {
      console.log(data);
      this.tags = data;
    });

    this.skillsService.get().subscribe((data: string[]) => {
      console.log(data);
      this.skills = data;
    });

    this.projectsService.get().subscribe((data: string[]) => {
      console.log(data);
      this.projects = data;
    });

    this.prioritiesService.get().subscribe((data: string[]) => {
      console.log(data);
      this.priorities = data;
    });

    this.datesService.get().subscribe((data: string[]) => {
      console.log(data);
      this.dates = data;
    });

  }

  onSelect(skill: string) {
    console.log(skill);
    this.findResources(skill);
  }

  findResources(skill: string) {
    console.log('skill changed');

    this.resourcesService.get(skill).subscribe((resources: string[]) => {
      console.log(resources);
      this.resources =  resources;
    });
  }

  createTask() {
    console.log(this.model);
    const newTask = new Task(
      this.model.tag,
      this.model.skill,
      this.model.project,
      this.model.priority,
      this.model.date,
      this.model.resource
    );
    this.tasks.push(newTask);
    console.log(this.tasks);
    this.model = new Task('', '', '', '', '', '');

  }
  get diagnostic() { return JSON.stringify(this.model); }
}
