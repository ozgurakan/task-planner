import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

const serviceUrl = environment.urls.resources;

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  constructor(private http: HttpClient) { }

  get(skill: string): Observable<string[]> {
    console.log('in resource service: ', skill);
    const term = skill.trim();

    if (serviceUrl.length === 0) {
      return of(['name mock 1', 'name mock 2']);
    }

    const options = term ? { params: new HttpParams().set('skill', term) } : {};
    return this.http.get<string[]>(serviceUrl, options);
  }
}
