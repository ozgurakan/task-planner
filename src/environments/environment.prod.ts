export const environment = {
  production: true,
  urls: {
    availability: '',
    date: 'https://9vf0760pfh.execute-api.us-east-1.amazonaws.com/prod/date',
    // date: 'https://xfiftw727k.execute-api.us-east-1.amazonaws.com/prod/date/',
    people: 'https://prcmukony2.execute-api.us-east-1.amazonaws.com/prd/people',
    // priorities: 'https://tjyhn4sute.execute-api.us-east-1.amazonaws.com/dev/priorities',
    // priorities : 'https://d7jj1zj18h.execute-api.us-east-1.amazonaws.com/prod/priorities',
    priorities: 'https://tjyhn4sute.execute-api.us-east-1.amazonaws.com/dev/priorities',
    projects: 'https://urkr7u7ngl.execute-api.us-east-1.amazonaws.com/prod/projects',
    // resources: 'https://l5zrc4rj16.execute-api.us-east-1.amazonaws.com/prod/resources',
    // resources: 'https://x2xz1j1mlj.execute-api.us-east-1.amazonaws.com/dev/resources',
    resources: 'https://l5zrc4rj16.execute-api.us-east-1.amazonaws.com/prod/resources',
    skills: 'https://cjepe2cz03.execute-api.us-east-1.amazonaws.com/prod/skills',
    // skills: '',
    // tags: 'https://l5zrc4rj16.execute-api.us-east-1.amazonaws.com/prod/tags',
    tags: 'https://fvcqy8de07.execute-api.us-east-1.amazonaws.com/prod/tags'
  }
};